ILP with the ProbLog engine
===========================

1. Testing a clause.
--------------------

A clause in ILP can be represented a disjunction of positive and negative literals.
The negative literals are called the body of the clause, and the positive ones are called the head.

.. math::
    
    h_1(X_1) \wedge h_2(X_2) \leftarrow b_1(X_1,X_2) \wedge b_2(X_2,X_3)

This clause can be translated into the following logic program:

.. code-block:: prolog

    score_not :- failing(_,_,_).
    score :- not(score_not).    
    failing(X1,X2,X3) :- b1(X1,X2), b2(X2,X3), not(h1(X1)), not(h2(X2)).
    
By evaluating the query ``score`` we get the score for the given clause.
If we want to find the substitutions for which the clause fails, we can use the query ``failing(X,Y,Z)``.

The general procedure is:

    * The clause failing has as arguments all variables that occur in the clause.
    * The body of the clause contains first the body literals, followed by the negated head literals. 

This evaluation method only works when all variables in the head literals have been assigned a value after evaluating the body literals, that is, when the clause in range restricted.

2. Testing subsumption by background.
-------------------------------------

As a pruning step, we want to eliminate clauses that follow directly from the background knowledge.


3. Refinement operator
----------------------

The refinement operator generates clauses that are in lexicographical order.

This means that:

    - negated literals come first (body), followed by positive literals (head)
    - literals within body and head appear in lexicographical order (by functor)

The initial clauses are defined as *cores* in the problem specification.
Care has to be taken that different cores don't produce the same rules.