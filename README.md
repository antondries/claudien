Claudien
========

Requirements
------------

- This package requires Python 2.7+.
- This package requires ProbLog v2.1.
    You can install it with `pip install problog --pre --user`

How to learn?
-------------

```bash
  ./learn.py claudien <datafile.pl>
```

The algorithm can be interrupted at any time with CTRL-C.
This will output all clauses learned up to that moment.

How to evaluate?
----------------

```bash
  ./learn.py eval <datafile.pl>
```

You can add a section `RULES` to the input file with the rules you want to evaluate.

How to generate rules?
----------------------

You can test your mode declarations by enumerating all possible rules.

```bash
  ./learn.py enum <datafile.pl>
```


Data format
-----------

See directory problems/ for examples.

The input file should contain a number of sections.
Sections are delimited by a line starting with
`% ===`

The sections are:

* **FACTS**: listing all facts that can occur in the examples, with a type indicator for each argument.
        The type indicator can be any Prolog atom.
* **MODES**: listing all terms that can occur in the clauses to be learned, with a mode indicator.
        The mode indicators are:
    - `+`   Use an existing variable.
    - `-`   Introduce a new variable on this position.
    - `c`   Use a constant.
Possible values for the constants are determined automatically from the data.
The initial refinement step also introduces new variables for the `+` modes.
* **BACKGROUND** [optional]: background theory in Prolog

* **RULES** [optional]: rules to evaluate (for `eval` mode)

* **(examples)**: Unnamed sections are considered to be the examples.

Example:

```prolog
% === FACTS ===
parent(p,p).
female(p).
male(p).
% === MODES ===
is_parent('+').
is_mother('+').
is_father('+').
% === BACKGROUND ===
mother(X,Y) :- female(X), parent(X,Y).
father(X,Y) :- male(X), parent(X,Y).
is_parent(X) :- parent(X,_).
is_mother(X) :- mother(X,_).
is_father(X) :- father(X,_).
% ============
parent(katrien,wout).
female(katrien).
% ============
parent(wim,wout).
male(wim).
% ============
parent(wim,wout).
parent(katrien,wout).
male(wim).
female(katrien).
```

License
-------

Copyright 2018 KU Leuven, DTAI Research Group

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
