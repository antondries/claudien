% === CORES ===
% \+parent(A,B).
% \+parent(A,A).
% \+female(A).
% \+male(A).
grandmother(A,B).
% === TYPES ===
parent(p,p).
female(p).
male(p).
grandmother(p,p).
% === MODES ===
\+parent(+,-).
\+parent(+,+).
\+male(+).
\+female(+).
grandmother(+,+).
% ============
male(anton).
male(erik).
male(pieter).
male(jaak).
male(edmond).
female(katrien).
female(maria).
female(julia).
female(francinne).
parent(erik,anton).
parent(francinne,anton).
parent(erik,katrien).
parent(francinne,katrien).
parent(erik,pieter).
parent(francinne,pieter).
parent(maria,erik).
parent(jaak, erik).
parent(julia,francinne).
parent(edmond,francinne).
grandmother(maria,anton).
grandmother(julia,anton).
grandmother(maria,katrien).
grandmother(julia,katrien).
grandmother(maria,pieter).
grandmother(julia,pieter).