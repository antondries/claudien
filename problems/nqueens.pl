% N-queens problem
% The correct constraints are:
%  q(A1,A2) /\ q(A1,A3) -> false  (no two queens on the same row)
%  q(A1,A2) /\ q(A3,A2) -> false  (no two queens on the same column)
%  q(A1,A2) /\ q(A3, A4) /\ gap_r(A1,A3,A5) /\ gap_c(A2,A4,A5) -> false.  (no two queens on the same diagonal) 

% === CORES ===
\+ q(R,C).
% === TYPES ===
pos.
q(r,c).
gap_r(r,r,g).
gap_c(c,c,g).
gap_rc(r,c,g).
% === BACKGROUND ===
gap_r(r(A),r(B),g(C)) :- gap(A,B,C).
gap_c(c(A),c(B),g(C)) :- gap(A,B,C).
gap_rc(r(A),c(B),g(C)) :- gap(A,B,C).
gap(R1,C1,G) :- G is abs(R1-C1).
% === MODES ===
q(+,+).
gap_r(+,+,+).
gap_c(+,+,+).
gap_rc(+,+,+).
\+ q(+,+).
\+ q(-,+).
\+ q(+,-).
\+ q(-,-).
\+ gap_r(+,+,+).
\+ gap_r(+,+,-).
\+ gap_c(+,+,+).
\+ gap_c(+,+,-).
\+ gap_rc(+,+,+).
\+ gap_rc(+,+,-).
%gap(+,+,+).
% === 
q(r(1),c(2)).
q(r(2),c(4)).
q(r(3),c(1)).
q(r(4),c(3)).
pos.
% ===
q(r(1),r(6)).
q(r(2),r(4)).
q(r(3),r(7)).
q(r(4),r(1)).
q(r(5),r(8)).
q(r(6),r(2)).
q(r(7),r(5)).
q(r(8),r(3)).
pos.



