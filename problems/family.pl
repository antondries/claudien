% === CORES ===
\+is_parent(A).
\+is_mother(A).
\+is_father(A).
is_parent(A).
% === TYPES ===
parent(p,p).
female(p).
male(p).
is_parent(p).
is_mother(p).
is_father(p).
person(p).
#(p).
% === MODES ===
is_parent(+).
is_mother(+).
is_father(+).
\+is_parent(+).
\+is_mother(+).
\+is_father(+).
% === BACKGROUND ===
mother(X,Y) :- female(X), parent(X,Y).
father(X,Y) :- male(X), parent(X,Y).
is_parent(X) :- parent(X,_).
is_mother(X) :- mother(X,_).
is_father(X) :- father(X,_).
% ============
parent(katrien,wout).
female(katrien).
% ============
parent(wim,wout).
male(wim).
% ============
parent(wim,wout).
parent(katrien,wout).
male(wim).
female(katrien).
