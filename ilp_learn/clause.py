from __future__ import print_function
from problog.formula import LogicFormula
from problog.engine import instantiate, UnifyError
from .engine import unify
from logging import getLogger as get_logger


class Clause(object):
    """Represents a clause. 
    
    This in abstract class; see EmptyClause and ExtendedClause for implementations.

    A clause is the basic representation of what is to be learned.
    It is defined as a set of literals.

    A clause is represented as a linked list of ExtendedClauses starting with an EmptyClause.

    """
    
    def __init__(self, literal, refinements=None, blacklist=None):
        self.literal = literal
        self._next_refinements = refinements
        self._update_refinements()

        # Evaluation statistics
        self.failings = None
        self.improvement = None
        self.blacklist = blacklist

    def _update_refinements(self):
        raise NotImplementedError()

    @property
    def literals(self):
        """Return a list of all literals in the clause.

        :return:
        :rtype: list[problog.logic.Term]
        """
        raise NotImplementedError()

    def __repr__(self):
        return ' \/ '.join(map(str, self.literals))
    
    def __str__(self):
        head_lits = [x for x in self.literals_head if x.functor != '#']
        body_lits = [x for x in list(map(abs, self.literals_body)) if x.functor != '#']
        if body_lits and head_lits:
            return '%s -> %s' % (' /\\ '.join(map(str, body_lits)),
                                 ' \\/ '.join(map(str, head_lits)))
        elif body_lits:
            return '%s -> false' % (' /\\ '.join(map(str, body_lits)))
        elif head_lits:
            return '%s' % (' \\/ '.join(map(str, head_lits)))
        else:
            return 'false'
        
    def __add__(self, literal):
        return ExtendedClause(self, literal)
            
    @property
    def literals_head(self):
        """Return a list of all positive literals in the clause.

        :return: list[problog.logic.Term]
        """
        return [lit for lit in self.literals if not lit.is_negated()]
    
    @property
    def literals_body(self):
        """Return a list of all negative literals in the clause.

        :return: list[problog.logic.Term]
        """
        return [lit for lit in self.literals if lit.is_negated()]
        
    def __contains__(self, lit):
        return lit in self.literals
            
    def __len__(self):
        return len(self.literals)
        
    @classmethod
    def from_list(cls, lst):
        """Create a Clause from a list of literals.

        :param lst: list of literals in the clause
        :type lst: list[problog.logic.Term]
        :return: list of literals
        :rtype: Clause
        """
        res = EmptyClause()
        for lit in lst:
            res += lit
        return res

    @classmethod
    def literals_from_logic(cls, rule):
        from problog.logic import AnnotatedDisjunction, And
        from problog.logic import Clause as ClauseP

        if isinstance(rule, AnnotatedDisjunction):
            heads = rule.heads
            body = rule.body
        elif isinstance(rule, ClauseP):
            if rule.head.functor == '_directive':
                heads = []
            else:
                heads = [rule.head]
            body = rule.body
        else:
            heads = [rule]
            body = None

        body_list = []
        while isinstance(body, And):
            body_list.append(body.op1)
            body = body.op2
        if body is not None:
            body_list.append(body)

        return [-lit for lit in body_list] + [lit for lit in heads]

    @classmethod
    def from_logic(cls, rule):
        """Create a Clause from a parsed ProbLog rule.

        :param rule: ProbLog rule representing a clause
        :type rule: problog.logic.Term
        :return:
        :rtype: Clause
        """
        lits = cls.literals_from_logic(rule)

        variables = None
        for lit in lits:
            if variables is None:
                variables = lit.variables()
            else:
                variables |= lit.variables()
        subst = {v: i for i, v in enumerate(variables)}
        
        for lit in lits:
            lit.apply(subst)
        
        lits = [lit.apply(subst) for lit in lits]
        return Clause.from_list(lits)

    @property
    def introduces_variables(self):
        """Indicate whether the last literal added to the clause introduces new variables.

        :return: True if the last added literal introduces a new variable
        :rtype: bool
        """
        raise NotImplementedError()

    @property
    def language(self):
        """Return the language specification according to which this rule was generated.

        :return: Language
        """
        raise NotImplementedError()

    @language.setter
    def language(self, value):
        raise NotImplementedError()

    def evaluate(self, examples, engine):
        """Evaluate the given rule on the given examples in the given engine.

        :param examples: examples on which to evaluate this clause
        :type examples: data.Interpretations
        :param engine: the evaluation engine to use
        :type engine: problog.engine.Engine
        :return: True if the rule is valid on all examples, False otherwise
        :rtype: bool
        """
        raise NotImplementedError()

    def refine(self):
        raise NotImplementedError()


class EmptyClause(Clause):
    
    def __init__(self, language=None):
        self._language = language
        self.variable_count = 0
        self.variable_types = []
        self.variable_names = []
        self.root = self
        Clause.__init__(self, literal=None)

    @property
    def literals(self):
        return []

    @property
    def language(self):
        return self._language

    @language.setter
    def language(self, value):
        self._language = value

    def _update_refinements(self):
        if self.language:
            self._next_refinements = list(self.language.refine_initial(self))
            
    def refine(self):
        for ref in self.language.refine_initial(self):
            yield ref
        # for i, ref in enumerate(self._next_refinements):
        #     yield ExtendedClause(self, ref, refinements=self._next_refinements[i+1:])

    @property
    def introduces_variables(self):
        return False
        
    def evaluate(self, examples, engine):
        self.failings = [[[]]] * len(examples)
        return False


class ExtendedClause(Clause):
    """
    
    Note: 
        Variables with the same name in different literals will be translated to distinct variables
        To reuse variables you should use integers instead of Var objects.
    
    """
    
    def __init__(self, parent, literal, refinements=None):
        self.parent = parent
        self.root = self.parent.root
        self.variable_count = self.parent.variable_count
        
        # Find new variables => those with names
        replace = {}
        vartypes = []
        litvars = literal.variables()
        
        for var in litvars:
            from .refinement import TypedVar
            if isinstance(var, TypedVar):
                if type(var.name) == int:
                    while var.name >= self.variable_count:
                        vartypes.append(None)
                        self.variable_count += 1
                    if var.name >= self.parent.variable_count:
                        vartypes[var.name-self.parent.variable_count] = var.type
                    replace[var] = var.name
                else:
                    if var.name not in replace:
                        vartypes.append(var.type)
                        replace[var.name] = self.variable_count
                        self.variable_count += 1
            elif type(var) == int:
                replace[var] = var
                self.variable_count = max(self.variable_count-1, var) + 1
            else:
                raise ValueError("Not a valid variable value '%s'!" % var)
        
        literal = literal.apply(replace)
        if vartypes:
            self.variable_types = parent.variable_types + vartypes
        else:
            self.variable_types = parent.variable_types
        Clause.__init__(self, literal, refinements)
        
    @property
    def language(self):
        return self.root.language

    @language.setter
    def language(self, value):
        self.root.language = value
        self._init_refinements()
        
    @property
    def literals(self):
        return self.parent.literals + [self.literal]
        
    def _get_new_variables(self):
        return list(range(self.parent.variable_count, self.variable_count))

    def _init_refinements(self):
        self._next_refinements = list(self.language.refine(self))
            # [ref for ref in self.language.refine(self) if abs(ref).functor >= abs(self.literal).functor]

    def _update_refinements(self):
        if self.language:
            new_variables = self._get_new_variables()    # list<int> : variable names
            assert(self._next_refinements is not None)
            update = list(self.language.refine(self, use_variables=new_variables))
            self._next_refinements += update
        
    def refine(self):
        for i, ref in enumerate(self._next_refinements):
            yield ExtendedClause(self, ref, refinements=self._next_refinements[i+1:])

    @property
    def introduces_variables(self):
        return self.variable_count > self.parent.variable_count
        
    def evaluate(self, examples, engine):
        if self.parent.failings is not None:
            failings = self.parent.failings
        else:
            self.parent.evaluate(examples, engine)
            failings = self.parent.failings
        
        rule_literal = abs(self.literal)
        if not self.literal.is_negated():
            head_literal = True
        else:
            head_literal = False
        fails = False
        new_failings = []
        improvement = False
        for example, failing in zip(examples, failings):
            if failing:
                if hasattr(example, '_ground'):
                    formula = example._ground
                    formula.clear_queries()
                else:
                    formula = LogicFormula()                     # Load from cache
                # formula = LogicFormula()
                
                new_failing = []
                for subst in failing:
                    if len(subst) < self.variable_count:
                        subst = subst + [None] * (self.variable_count - len(subst))
                        improvement = True  # adds a variable
                    literal = instantiate(rule_literal, subst)
                    formula = engine.ground(example.database, literal, target=formula, label='query')
                    for q, i in formula.queries():
                        subst_new = subst[:]
                        try:
                            unify(q, rule_literal, subst_new)
                            if len(set(subst_new)) == len(subst_new):
                                # TODO check subst_new is all different
                                if head_literal and i is None:
                                    if None in subst_new:
                                        get_logger('claudien').error(' '.join(map(str, (list(example.database), self, failing, formula))))
                                        raise RuntimeError('This shouldn\'t happen!')
                                    
                                    new_failing.append(subst_new)
                                elif not head_literal and i == 0:
                                    new_failing.append(subst_new)
                        except UnifyError:
                            pass
                example._ground = formula
                new_failings.append(new_failing)

                if new_failing:
                    if len(new_failing) < len(failing):
                        improvement = True
                    fails = True
                else:
                    assert failing
                    improvement = True  # failing eliminated
            else:
                new_failings.append([])
        self.failings = new_failings
        self.improvement = improvement
        return not fails
